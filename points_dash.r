#######################################################################
#
# points_dash.r    02 May 2018
#
# Author: Gregory Garner (ggarner@princeton.edu)
#
# Function that appends dashes as points on an existing plot.
#
# To use this function, simply source this file:
#   source("points_dash.r")
#
# Version History:
#   1.0 - 02 May 2018 - Initial coding (G.G.)
#
# THIS CODE IS PROVIDED AS-IS WITH NO WARRANTY (NEITHER EXPLICIT
# NOT IMPLICIT).  I SHARE THIS CODE IN HOPES THAT IT IS USEFUL, 
# BUT I AM NOT LIABLE FOR THE BEHAVIOR OF THIS CODE IN YOUR OWN
# APPLICATION.  YOU ARE FREE TO SHARE THIS CODE SO LONG AS THE
# AUTHOR(S) AND VERSION HISTORY REMAIN INTACT.
#
#######################################################################

#######################################################################
# Function Name: points.dash()
# Parameters:
#   x - Vector of x-coordinates
#   y - Vector of y-coordinates
#   len - Length of dashes (default = 0.01)
#   ... - Other parameters to be passed to the segments() function
#
# See the accompanying "points_dash_example.r" file for examples.
#
#######################################################################
points.dash <- function(x, y=x, len=0.01, ...){
  x.nfc <- grconvertX(x, from="user", to="nfc")
  start.x <- grconvertX(x.nfc - (len/2), from="nfc", to="user")
  end.x <- grconvertX(x.nfc + (len/2), from="nfc", to="user")
  segments(x0 = start.x, y0 = y, x1 = end.x, y1 = y, lty=1, ...)
}