## points_dash.r    02 May 2018##

 Author: Gregory Garner (ggarner@princeton.edu)

 Function that appends dashes as points on an existing plot.

 To use this function, simply source this file:

```
#!r

   source("points_dash.r")
```


### Version History:###

*    1.0 - 02 May 2018 - Initial coding (G.G.)

 **THIS CODE IS PROVIDED AS-IS WITH NO WARRANTY (NEITHER EXPLICIT
 NOT IMPLICIT).  I SHARE THIS CODE IN HOPES THAT IT IS USEFUL, 
 BUT I AM NOT LIABLE FOR THE BEHAVIOR OF THIS CODE IN YOUR OWN
 APPLICATION.  YOU ARE FREE TO SHARE THIS CODE SO LONG AS THE
 AUTHOR(S) AND VERSION HISTORY REMAIN INTACT.**

### Function Name: points.dash()###

Function Definition:

```
#!r

points.dash <- function(x, y=x, len=0.01, ...)

```

Parameters:
*    x - Vector of x-coordinates

*    y - Vector of y-coordinates

*    len - Length of dashes (default = 0.01)

*    ... - Other parameters to be passed to the segments() function


### Description###
This function simply wraps around the "segments()" function for adding
dashes (with the option of varying lengths) to an existing plot. See
"segments()" for additional parameters and options.

** See the accompanying "points_dash_example.r" file for usage and examples.**