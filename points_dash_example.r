##################################################
#
# points_dash_example.r   May 02, 2018
#
# Example use of the points.dash() function in
# points_dash.r script.
#
##################################################

# Source the legend function file
source("points_dash.r")

# Manually set up a plotting region
plot.new()
plot.window(xlim=c(1,10), ylim=c(1,10))
axis(1); axis(2); box()

# Append the dashes at particular point locations
points.dash(1:10)

# Append dashes with different lengths
for(i in 1:9){
  points.dash(x=i+0.5, y=i+0.5, len=0.01*i, col="red")
}

# Note: Additional parameters are passed to the
#       "segments()" function.